#!/usr/bin/env node

const logger = require('@pubsweet/logger')
const {
  model: BookCollection,
} = require('editoria-data-model/src/bookCollection')
const {
  model: BookCollectionTranslation,
} = require('editoria-data-model/src/bookCollectionTranslation')

const seed = async () => {
  // logger.info('fsdfdsfd', BookCollection)
  let collection
  // , translation

  await new BookCollection().save().then(res => (collection = res))

  logger.info(collection)

  await new BookCollectionTranslation({
    collectionId: collection.id,
    languageIso: 'en',
    title: 'Books',
  })
    .save()
    .then(res => logger.info(res))
}

seed()
